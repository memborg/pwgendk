use crate::words::Word;
use rand::{
    distributions::{Distribution, Standard},
    Rng,
};

#[derive(Debug)]
enum ToUpper {
    No,
    Yes,
}

impl ToUpper {
    fn yes(&self) -> bool {
        matches!(*self, ToUpper::Yes)
    }
}

impl Distribution<ToUpper> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> ToUpper {
        match rng.gen_range(0..2) {
            1 => ToUpper::Yes,
            _ => ToUpper::No,
        }
    }
}

pub fn get_sentence(words: &[Word]) -> String {
    let mut rng = rand::thread_rng();

    let specials = [
        ".", "!", "?", "?!", " :-)", " :-(", ":-[", ":-]", ":-}", ":-{",
    ];

    let _wordcls = [
        "pron.", "adj.", "sb.", "præp.", "vb.", "pron.", "adj.", "sb.",
    ];

    let wordcls = ["pron.", "adj.", "sb.", "præp.", "vb."];
    let mut ws: Vec<String> = Vec::<String>::new();

    let mut starts_with_num = false;
    let mut did_to_upper = false;
    for wc in wordcls.iter() {
        let list: Vec<&Word> = words.iter().filter(|x| x.typ.contains(*wc)).collect();
        let i = rng.gen_range(0..list.len()) as usize;
        let w = list.get(i).unwrap().word.clone();
        let nw = word_to_str_number(w);
        let mut w = nw.0;

        if wc == &"pron." {
            starts_with_num = nw.1;
        }

        let to_upper: ToUpper = rand::random();
        if to_upper.yes() && !did_to_upper {
            w = w.to_uppercase();
            did_to_upper = true;
        }

        if wc == &"adj." && !starts_with_num {
            w = format!("{} {}", rng.gen_range(0..100), w);
        }

        ws.push(w);
    }

    let i = rng.gen_range(0..specials.len()) as usize;
    let w = specials.get(i).unwrap();

    let sentence = ws.join(" ") + w;

    crate::words::uppercase_first_letter(&sentence)
}

fn word_to_str_number(word: String) -> (String, bool) {
    let mut rng = rand::thread_rng();
    let zeroes = ["nul", "ingen", "ingenting", "intet"];
    if zeroes.contains(&word.as_str()) {
        return ("0".into(), true);
    }

    let singles = ["en", "et"];
    if singles.contains(&word.as_str()) {
        return ("1".into(), true);
    }

    let twos = ["begge", "andet", "andet"];
    if twos.contains(&word.as_str()) {
        return ("2".into(), true);
    }

    let words = [
        "nogle",
        "nogen",
        "noget",
        "os",
        "allesammen",
        "ethvert",
        "vi",
    ];
    if words.contains(&word.as_str()) {
        return (rng.gen_range(0..100).to_string(), true);
    }

    (word, false)
}
