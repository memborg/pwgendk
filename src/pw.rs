use rand::{thread_rng, Rng};

pub fn get_pw() -> String {
    let mut rng = thread_rng();

    let chars: Vec<_> = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890*$-+=!"
        .chars()
        .collect();
    let len = rng.gen_range(8..16);

    let mut i = 0;
    let mut pw = String::new();

    loop {
        if i < len {
            let c = rng.gen_range(0..chars.len());

            if let Some(s) = chars.get(c) {
                if !pw.contains(*s) {
                    pw.push(*s);
                    i += 1;
                }
            }
        } else {
            break;
        }
    }

    pw
}
