#![recursion_limit = "1024"]
extern crate wee_alloc;

// Use `wee_alloc` as the global allocator.
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

use rand::{thread_rng, Rng};
use reqwasm::http::Request;
use yew::prelude::*;

mod leet;
mod pw;
mod sentence;
mod words;

#[function_component(App)]
fn app() -> Html {
    let mut rng = thread_rng();

    let sentence: UseStateHandle<String> = use_state(String::new);
    let sentence_len: UseStateHandle<usize> = use_state(|| 0);

    let pw2: UseStateHandle<String> = use_state(String::new);
    let pw2_len: UseStateHandle<usize> = use_state(|| 0);

    let leetpw: UseStateHandle<String> = use_state(String::new);
    let leetpw_len: UseStateHandle<usize> = use_state(|| 0);

    {
        let sentence = sentence.clone();
        let sentence_len = sentence_len.clone();

        let pw2 = pw2.clone();
        let pw2_len = pw2_len.clone();

        let leetpw = leetpw.clone();
        let leetpw_len = leetpw_len.clone();

        use_effect_with_deps(
            move |_| {
                let sentence = sentence.clone();
                let sentence_len = sentence_len.clone();

                let pw2 = pw2.clone();
                let pw2_len = pw2_len.clone();

                let leetpw = leetpw.clone();
                let leetpw_len = leetpw_len.clone();

                wasm_bindgen_futures::spawn_local(async move {
                    let fetched_words: Vec<words::Word> = Request::get("/words.json?v=1.5.9")
                        .send()
                        .await
                        .unwrap()
                        .json()
                        .await
                        .unwrap();

                    let s = sentence::get_sentence(&fetched_words);
                    sentence.set(s.clone());
                    sentence_len.set(s.len());

                    let w = format!("{}{}", words::get_word(&fetched_words, 10), rng.gen_range(0..100));
                    pw2.set(w.clone());
                    pw2_len.set(w.len());

                    let l = leet::Leet::new();
                    let ww = l.to_leet_speak(words::get_word(&fetched_words, 12));
                    leetpw.set(ww.clone());
                    leetpw_len.set(ww.len());
                });
                || ()
            },
            (),
        );
    }

    let pw = pw::get_pw();
    let pw_len = pw.len();

    html! {
        <div class="container-fluid">
            <main class="col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content">
            <h1>{"Kodeord generator"}</h1>
            <form>
            <div class="form-group">
            <label for="pw">{"Kode sætning"}</label>
            <input class="form-control" id="pw" type="text" name="sentence" value={(*sentence).clone()}/>
            <small class="form-text text-muted">{"Længde: "}{*sentence_len}</small>
            </div>
            <div class="form-group">
            <label for="pw2">{"Kodeord"}</label>
            <input class="form-control" id="pw2" type="text" name="pw" value={pw}/>
            <small class="form-text text-muted">{"Længde: "}{pw_len}</small>
            </div>
            <div class="form-group">
            <label for="pw2">{"Kodeord 2"}</label>
            <input class="form-control" id="pw2" type="text" name="pw2" value={(*pw2).clone()}/>
            <small class="form-text text-muted">{"Længde: "}{*pw2_len}</small>
            </div>
            <div class="form-group">
            <label for="pw2">{"1337 kodeord"}</label>
            <input class="form-control" id="leetpw" type="text" name="leetpw" value={(*leetpw).clone()}/>
            <small class="form-text text-muted">{"Længde: "}{*leetpw_len}</small>
            </div>
            </form>
            </main>
        </div>
    }
}

fn main() {
    wasm_logger::init(wasm_logger::Config::default());
    yew::start_app::<App>();
}
