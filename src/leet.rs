use rand::{thread_rng, Rng};
use std::collections::HashMap;

#[derive(Debug, Clone, Default)]
pub struct Leet {}

impl Leet {
    pub fn new() -> Leet {
        Leet::default()
    }

    pub fn to_leet_speak(&self, word: String) -> String {
        let leet_chars = self.get_hashmap();

        let mut it = word.chars().peekable();
        let mut sec = String::new();

        while let Some(ch) = it.peek() {
            sec.push(self.convert(*ch, &leet_chars));
            it.next().unwrap();
        }

        crate::words::uppercase_first_letter(&sec)
    }

    fn get_hashmap(&self) -> HashMap<char, Vec<char>> {
        HashMap::from([
            ('a', vec!['4']),
            ('b', vec!['8']),
            ('e', vec!['3']),
            ('g', vec!['9']),
            ('h', vec!['#']),
            ('i', vec!['1', '!']),
            ('l', vec!['1', '!']),
            ('o', vec!['0']),
            ('s', vec!['5']),
            ('t', vec!['7']),
        ])
    }

    fn convert(&self, c: char, chars: &HashMap<char, Vec<char>>) -> char {
        let c = c.to_lowercase().to_string().chars().nth(0).unwrap();
        if let Some(val) = chars.get(&c) {
            let mut rng = thread_rng();
            let i = rng.gen_range(0..val.len());

            return val[i];
        }

        c
    }
}
