use rand::{thread_rng, Rng};
use serde::Deserialize;

#[derive(Deserialize, Clone, Debug)]
pub struct Word {
    pub word: String,
    pub typ: String,
}

pub fn get_word(words: &[Word], len: usize) -> String {
    let list: Vec<&Word> = words.iter().filter(|x| x.word.len() == len).collect();

    let mut rng = thread_rng();
    let i = rng.gen_range(0..list.len());

    uppercase_first_letter(&list[i].word)
}

pub fn uppercase_first_letter(s: &str) -> String {
    s[0..1].to_uppercase() + &s[1..]
}
